package com.fagainv.tests;

import com.fagainv.business.PersonRules;
import com.fagainv.business.impl.PersonRulesImpl;
import com.fagainv.model.Habit;
import com.fagainv.model.Health;
import com.fagainv.model.Person;
import java.util.Calendar;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Victorino Pastrana Franzoni Jan 25, 2018 8:40:27 AM
 * @since 1.0
 * @version 1.0
 * 
 */
@RunWith(Arquillian.class)
public class PersonRulesTest {
    
  
    
    @Inject
     private PersonRules personRules;
    

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClasses(PersonRules.class, PersonRulesImpl.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return jar;
    }
    
    
    @Test
    public void instance(){
        Assert.assertNotNull(personRules);
        
    }
    
    // build person 
    public Person createPerson() {
        Person person = new Person();
        person.setId(1);
        person.setName("Mr. Gomes");
        person.setGender("M");
        Calendar cal = Calendar.getInstance();
        cal.set(1984, 0, 1, 0, 0);
        person.setAge(cal.getTime());
        Habit habits = new Habit();
        habits.setSmoking(Boolean.FALSE);
        habits.setAlcohol(Boolean.TRUE);
        habits.setExercise(Boolean.TRUE);
        habits.setDrugs(Boolean.FALSE);
        person.setHabit(habits);
        Health health = new Health();
        health.setHypertension(Boolean.FALSE);
        health.setBloodPressure(Boolean.FALSE);
        health.setBloodSugar(Boolean.FALSE);
        health.setOverweight(Boolean.TRUE);
        person.setHealth(health);
        return person;
    }
    
    @Test
    public void mrGomes() {
        Person person = createPerson();
        personRules.setPerson(person);
        Long expected = 6856l;
        Long actual = personRules.getHealthInsurance();
        Assert.assertEquals(expected, actual);
    }
    
    
    @Test
    public void invalid_mrGomes() {
        Person person = createPerson();
        personRules.setPerson(person);
        Long expected = 6876l;
        Long actual = personRules.getHealthInsurance();
        Assert.assertNotEquals(expected, actual);
    }

}

package com.fagainv.configapp;

import com.fagainv.business.PersonRules;
import com.fagainv.business.impl.PersonRulesImpl;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author Victorino Pastrana Franzoni Jan 25, 2018 11:35:53 AM
 * @since 1.0
 * @version 1.0
 * 
 */
public class ConfigApp extends ResourceConfig {

    
    public ConfigApp() {
        System.out.println("ConfigApp....");
        
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(PersonRulesImpl.class).to(PersonRules.class);
            }
        });
        packages(true, "com.fagainv.rs");
        
    }
    
    
}
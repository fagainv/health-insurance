package com.fagainv.rs;

import com.fagainv.business.PersonRules;
import com.fagainv.model.Person;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Victorino Pastrana Franzoni Jan 25, 2018 11:31:25 AM
 * @since 1.0
 * @version 1.0
 *
 */
@Path("health")
public class HealthRs {

    @Inject
    private PersonRules personRules;

    @POST
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response add(Person person) {
        System.out.println("personRules: " + personRules);
        personRules.setPerson(person);
        return Response.ok().entity("Health insurance: " + personRules.getHealthInsurance()).build();
    }

}

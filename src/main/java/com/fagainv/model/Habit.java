package com.fagainv.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Victorino Pastrana Franzoni Jan 25, 2018 8:13:50 AM
 * @since 1.0
 * @version 1.0
 *
 */
@Entity
@Table(name = "HABITS")
@SequenceGenerator(name = "habit_seq", allocationSize = 5, initialValue = 1)
public class Habit implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "health_seq")
    @Column(name = "ID")
    private Integer id;
    @Column(name = "SMOKING", length = 1)
    private Boolean smoking;
    @Column(name = "ALCOHOL", length = 1)
    private Boolean Alcohol;
    @Column(name = "EXERCISE", length = 1)
    private Boolean exercise;
    @Column(name = "DRUGS", length = 1)
    private Boolean Drugs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSmoking() {
        return smoking;
    }

    public void setSmoking(Boolean smoking) {
        this.smoking = smoking;
    }

    public Boolean getAlcohol() {
        return Alcohol;
    }

    public void setAlcohol(Boolean Alcohol) {
        this.Alcohol = Alcohol;
    }

    public Boolean getExercise() {
        return exercise;
    }

    public void setExercise(Boolean exercise) {
        this.exercise = exercise;
    }

    public Boolean getDrugs() {
        return Drugs;
    }

    public void setDrugs(Boolean Drugs) {
        this.Drugs = Drugs;
    }
    
    

}

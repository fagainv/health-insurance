package com.fagainv.model;

/**
 *
 * @author Victorino Pastrana Franzoni Jan 25, 2018 9:13:50 AM
 */
public enum Gender {
    MALE("M"), FEMALE("F");

    private final String code;

    Gender(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}

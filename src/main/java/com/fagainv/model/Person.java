
package com.fagainv.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Victorino Pastrana Franzoni Jan 25, 2018 8:13:26 AM
 * @since 1.0
 * @version 1.0
 * 
 */
@Entity
@Table(name="PERSONS")
@SequenceGenerator(name = "person_seq", allocationSize = 5, initialValue = 1)
public class Person implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "person_seq")
    @Column(name="ID")
    private Integer id;
    @Column(name="NAME", length = 160, nullable = false)
    
    private String name;
    @Column(name="GENDER", length = 1, nullable = false)
    private String gender;
    @Column(name="AGE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date age;
    @OneToOne
    @JoinColumn(name="HEALTH_ID", nullable = false, unique = true)
    private Health health;
    @OneToOne
    @JoinColumn(name="HABIT_ID", nullable = false, unique = true)
    private Habit habit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getAge() {
        return age;
    }

    public void setAge(Date age) {
        this.age = age;
    }

    public Health getHealth() {
        return health;
    }

    public void setHealth(Health health) {
        this.health = health;
    }

    public Habit getHabit() {
        return habit;
    }

    public void setHabit(Habit habit) {
        this.habit = habit;
    }
    
    
}

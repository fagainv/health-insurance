package com.fagainv.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Victorino Pastrana Franzoni Jan 25, 2018 8:13:43 AM
 * @since 1.0
 * @version 1.0
 *
 */
@Entity
@Table(name = "HEALTHS")
@SequenceGenerator(name = "health_seq", allocationSize = 5, initialValue = 1)
public class Health implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "health_seq")
    @Column(name = "ID")
    private Integer id;
    @Column(name = "HIPERTENSION", length = 1)
    private Boolean hypertension;
    @Column(name = "BLOOD_PRESSURE", length = 1)
    private Boolean bloodPressure;
    @Column(name = "BLOOD_SUGAR", length = 1)
    private Boolean bloodSugar;
    @Column(name = "OVERWEIGHT", length = 1)
    private Boolean overweight;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getHypertension() {
        return hypertension;
    }

    public void setHypertension(Boolean hypertension) {
        this.hypertension = hypertension;
    }

    public Boolean getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(Boolean bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public Boolean getBloodSugar() {
        return bloodSugar;
    }

    public void setBloodSugar(Boolean bloodSugar) {
        this.bloodSugar = bloodSugar;
    }

    public Boolean getOverweight() {
        return overweight;
    }

    public void setOverweight(Boolean overweight) {
        this.overweight = overweight;
    }
    
    

}

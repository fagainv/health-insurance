package com.fagainv.business.impl;

import com.fagainv.business.PersonRules;
import com.fagainv.model.Gender;
import com.fagainv.model.Person;
import java.util.Calendar;
import java.util.Date;

/**
 * Base premium for anyone below the age of 18 years = Rs. 5,000
 *
 * % increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% |
 * 35-40 -> +10% | 40+ -> 20% increase every 5 years
 *
 * Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for
 * Males
 *
 * Pre-existing conditions (Hypertension | Blook pressure | Blood sugar |
 * Overweight) -> Increase of 1% per condition Habits
 *
 *
 * Good habits (Daily exercise) -> Reduce 3% for every good habit Bad habits
 * (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit
 *
 * @author Victorino Pastrana Franzoni Jan 25, 2018 8:37:42 AM
 * @since 1.0
 * @version 1.0
 *
 */
public class PersonRulesImpl implements PersonRules {

    private Person person;

    @Override
    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public Long getHealthInsurance() {
        Double basePrime = ageIncrement();
        Double total = 0.0;

        if (person.getGender().equals(Gender.MALE.getCode())) {
            basePrime += basePrime * 0.02;
        }
        if (person.getHabit().getAlcohol()) {
            total += basePrime * 0.03;
        }
        if (person.getHabit().getDrugs()) {
            total += basePrime * 0.03;
        }
        if (person.getHabit().getSmoking()) {
            total += basePrime * 0.03;
        }
        if (person.getHabit().getExercise()) {
            total -= basePrime * 0.03;
        }
        if (person.getHealth().getBloodPressure()) {
            total += basePrime * 0.01;
        }
        if (person.getHealth().getBloodSugar()) {
            total += basePrime * 0.01;
        }
        if (person.getHealth().getHypertension()) {
            total += basePrime * 0.01;
        }
        if (person.getHealth().getOverweight()) {
            total += basePrime * 0.01;
        }
        total += basePrime;
        return Math.round(total);
    }

    private Double ageIncrement() {
        Double total = 5000.0;
        Integer age = calculateAge();

        if (age < 18) {
            return total;
        }

        total += total * 0.1;

        for (int i = 25; i <= age; i++) {
            if ((i % 5) == 0) {
                total += total * 0.1;
            }
        }
        return total;
    }

    private Integer calculateAge() {

        Calendar a = getCalendar(person.getAge());
        Calendar b = Calendar.getInstance();
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH)
                || (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    private Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

}

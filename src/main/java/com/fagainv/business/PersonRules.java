package com.fagainv.business;

import com.fagainv.model.Person;

/**
 *
 * @author Victorino Pastrana Franzoni, Jan 25, 2018 8:31:23 AM
 * @since 1.0
 * @version 1.0
 */
public interface PersonRules {
    
    void setPerson(Person person);
    Long getHealthInsurance();
    
    

}
